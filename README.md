# u-alert

> vue.js alert plugin

## Installing

Set as package.json dependency: "u-alert": "bitbucket:Uhanov/u-alert",

## Usage

import uAlert from 'u-alert'

Vue.use(uAlert)

this.$alert.show({
  message: 'some message',
  duration: 1500
})