import uAlert from './alert.vue'

const uAlertWrapper = {
  install (Vue) {
    if (this.installed) return
    this.installed = true
    Vue.component('vue-alert', uAlert)
    Object.defineProperties(Vue.prototype, {
      $alert: {
        get () {
          let el = this
          while (el) {
            for (let i = 0; i < el.$children.length; i++) {
              const child = el.$children[i]

              if (child.$options._componentTag === 'vue-alert') {
                return child
              }
            }
            el = el.$parent
          }
          return null
        }
      }
    })
  }
}

export default uAlertWrapper
